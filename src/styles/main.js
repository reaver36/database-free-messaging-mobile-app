import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    header: {
        backgroundColor: '#e1e8ee'
    },
    heading: {
        fontSize: 18,
        fontWeight: 'bold'
    }
});