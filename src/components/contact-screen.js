import React from 'react';
import {Alert, View} from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
import {Database, SENT_RECIEVE} from '../lib/realm-connection';
import SocketClient from '../lib/socket-client';
import Icon from 'react-native-vector-icons/FontAwesome';

let onlineTimer;

export default class ContactScreen extends React.Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        const { params } = navigation.state;

        return {
        title: params ? params.contact.nickname : '',
        headerRight: <View style={{paddingRight:10}}><Icon name="circle" size={15} color={params.onlineColor ? params.onlineColor : "#ff0000"}/></View>,
        /* These values are used instead of the shared configuration! */
        headerStyle: {
            backgroundColor: navigationOptions.headerTintColor,
        },
        headerTintColor: navigationOptions.headerStyle.backgroundColor,
        };
    };

    constructor(params){
        super(params);
        const contact = this.props.navigation.state.params.contact;

        this.setOnlineStatus = this.setOnlineStatus.bind(this);
        contact.messages.addListener(()=>{
            let messages = [];
            let me = {name: "Me", _id: 0};
            let them = {name: contact.nickname, _id: 1};
            contact.messages.sorted('createdAt', true).forEach(function(msg){
                let message = {...msg, _id: msg.id};
                if(msg.sentRecieve == SENT_RECIEVE.SENT){
                    message.user = me;
                }
                else{
                    message.user = them;
                }
                messages.push(message);
            });
            this.setState({messages});
          });

        this.state = {messages: [], contact};
    }

    setOnlineStatus(){
        SocketClient.isOnline(this.state.contact.identifier, (online)=>{
            if(online){
                this.props.navigation.setParams({onlineColor: "#00ff00"});
            }
            else{
                this.props.navigation.setParams({onlineColor: "#ff0000"});
            }
        });
        onlineTimer = setTimeout(this.setOnlineStatus, 1000);
    }

    componentWillMount(){
        this.setOnlineStatus();
    }

    componentWillUnmount(){
        clearTimeout(onlineTimer);
    }

    onSend(messages = []) {
        messages.forEach((message) => {
            SocketClient.sendMessage(message.text, this.state.contact.identifier, (response)=>{
                if(response){
                    Database.SaveMessage(this.state.contact, {...message, sentRecieve: SENT_RECIEVE.SENT});
                }
                else{
                    Alert.alert(
                        "Unable to send message",
                        "",
                        [
                            {text: "OK"}
                        ]
                    );
                }
            });
        });
    }

    render() {
        return (
            <GiftedChat
                messages={this.state.messages}
                onSend={messages => this.onSend(messages)}
                user={{
                _id: 0,
                }}
            />
        );
    }
}