import React from 'react';
import {View} from 'react-native';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import SocketClient from '../lib/socket-client';
import {SENT_RECIEVE} from '../lib/realm-connection';
import Swipeout from 'react-native-swipeout';

let onlineTimer;

export default class ContactList extends React.Component {

    constructor(props){
        super(props);
        this.state = {contextMenuTop: 0, onlineStatus: {}};
        this.setOnlineStatus = this.setOnlineStatus.bind(this);
    }

    setOnlineStatus(){
        this.props.contacts.forEach(contact => {
            SocketClient.isOnline(contact.identifier, (online)=>{
                this.setState(previousState => {
                    if(online){
                        previousState.onlineStatus[contact.identifier] = true;
                    }
                    else{
                        previousState.onlineStatus[contact.identifier] = false;
                    }
                    return {onlineStatus: previousState.onlineStatus};
                });
            });
        });
        // Only start timer after isOnline check has started for all contact
        onlineTimer = setTimeout(this.setOnlineStatus, 1000);
    }

    componentWillMount(){
        this.setOnlineStatus();
    }

    componentWillUnmount(){
        clearTimeout(onlineTimer);
    }

    render(){
        let contactList = [];
        this.props.contacts.forEach(contact => {
            let swipeoutBtns = [
                {
                    text:'Edit',
                    type: 'primary',
                    onPress:()=>this.props.editContactHandler(contact)
                },
                {
                    text:'Delete',
                    type: 'delete',
                    onPress:()=>this.props.deleteContactHandler(contact)
                },
                {
                    text:"Clear Msgs",
                    type: 'Secondary',
                    onPress:()=>this.props.clearMessagesHandler(contact)
                },
            ];
            contactList.push(
                //buttonWidth used till wrap bug fixed
                <Swipeout right={swipeoutBtns} key={contact.identifier} buttonWidth={100}>
                    <ListItem
                        title={contact.nickname}
                        textInput={true}
                        badge={{ value: contact.messages.filtered("sentRecieve == $0", SENT_RECIEVE.RECIEVE).length }}
                        leftIcon={<Icon name="circle" size={15} color={this.state.onlineStatus[contact.identifier] ? "#00ff00" : "#ff0000"}/>}
                        bottomDivider={true}
                        chevron
                        onPress={()=>this.props.onContactPress(contact)} />
                </Swipeout>
            );
        });
        return (
            <View>
                {contactList}
            </View>
        );
    }
}