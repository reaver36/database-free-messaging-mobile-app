import React from 'react';
import {BackHandler, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Overlay, Button, Input} from 'react-native-elements';

export default class EditNickname extends React.Component {
    constructor(props){
        super(props);

        this.onEditContactPressed = this.onEditContactPressed.bind(this);
        this.onNicknameChanged = this.onNicknameChanged.bind(this);
        this.state = { nickname: null, nicknameErrMsg: null};
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.props.backHandler);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.props.backHandler);
    }

    onNicknameChanged(val){
        this.setState((prevState)=>{
            prevState.nickname = val;
            return prevState;
        });
    }

    onEditContactPressed(){
        let pass = true;
        if(!this.state.nickname.length){
            this.setState({nicknameErrMsg: "Required"});
            pass = false;
        }
        else{
            this.setState({nicknameErrMsg: null});
        }

        if(pass){
            this.props.editContactHandler(this.state.nickname);
        }
    }

    render() {
        let nicknameErrMsg = this.state.nicknameErrMsg;
        return (
            <Overlay isVisible={this.props.isVisible} height={150}>
                <View flex={2}>
                    <View>
                        <Input placeholder="Friend's nickname" leftIcon={<Icon name="pencil" />} value={this.state.nickname != null ? this.state.nickname : this.props.nickname} errorMessage={nicknameErrMsg} onChangeText={this.onNicknameChanged}/>
                    </View>
                    <Button title="Update nickname" onPress={this.onEditContactPressed} />
                </View>
                <View flex={1} justifyContent="flex-end" alignContent="flex-end">
                    <Button title="Cancel" onPress={this.props.closeHandler} />
                </View>
            </Overlay>
        );
    }
}