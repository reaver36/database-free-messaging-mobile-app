import React from 'react';
import { View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class AddContactButton extends React.Component {

    render(){
        return (
        <View style={{ flexDirection: "row", padding: 5 }}>
            <TouchableOpacity style={{padding: 10 }} onPress={() => this.props.onAddContactPress()}>
                <Icon name="user-plus" size={15} color="#383838" />
            </TouchableOpacity>
            <TouchableOpacity style={{ justifyContent: "center", alignContent: "center" }} onPress={() => this.props.onAddContactPress()}>
                <Text alignItems="center" style={{ fontSize: 15 }}>Add new friend</Text>
            </TouchableOpacity>
        </View>
      );
    }
}