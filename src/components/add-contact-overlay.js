import React from 'react';
import {BackHandler, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Overlay, Button, Input, Text} from 'react-native-elements';
import DeviceInfo from 'react-native-device-info';
let moment = require('moment');
import styles from '../styles/main';
import SocketClient from '../lib/socket-client';

export default class AddContact extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            contact: {contactCode: "", nickname: ""},
            contactCodeErrMsg: null,
            nicknameErrMsg: null,
            myContactCode: "",
            myContactCodeExpiry: null
        };
        this.onContactCodeChanged = this.onContactCodeChanged.bind(this);
        this.onNicknameChanged = this.onNicknameChanged.bind(this);
        this.onAddContactPressed = this.onAddContactPressed.bind(this);
        this.onGetContactCodePressed = this.onGetContactCodePressed.bind(this);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.props.backHandler);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.props.backHandler);
    }

    
    onContactCodeChanged(val){
        this.setState((prevState)=>{
            prevState.contact.contactCode = val;
            return prevState;
        });
    }

    onNicknameChanged(val){
        this.setState((prevState)=>{
            prevState.contact.nickname = val;
            return prevState;
        });
    }

    onAddContactPressed(){
        let pass = true;
        if(!this.state.contact.nickname.length){
            this.setState({nicknameErrMsg: "Required"});
            pass = false;
        }
        else{
            this.setState({nicknameErrMsg: null});
        }

        if(!this.state.contact.contactCode.length){
            this.setState({contactCodeErrMsg: "Required"});
            pass = false;
        }
        else{
            this.setState({contactCodeErrMsg: null});
        }
        if(pass){
            this.props.addContactHandler(this.state.contact);
            this.setState({contact: {contactCode: "", nickname: ""}});
        }
    }

    onGetContactCodePressed(){
        SocketClient.getContactCode(DeviceInfo.getUniqueID(), (contactCode) => {
            this.setState({myContactCode: contactCode.code, myContactCodeExpiry: "Expires: " + moment.utc(contactCode.expiry).local().format("h:mm A")});
        });
    }
    
    render() {
        let nicknameErrMsg = this.state.nicknameErrMsg;
        let contactCodeErrMsg = this.state.contactCodeErrMsg;
        return (
            <Overlay isVisible={this.props.isVisible}>
                <View flex={3}>
                    <Text style={styles.heading}>Add a Friend Using Their Code</Text>
                    <View paddingBottom={20}>
                        <Input placeholder="Friend's nickname" leftIcon={<Icon name="pencil" />} value={this.state.contact.nickname} errorMessage={nicknameErrMsg} onChangeText={this.onNicknameChanged}/>
                        <Input placeholder="Friend's contact code" leftIcon={<Icon name="user" />} keyboardType="numeric" value={this.state.contact.contactCode} errorMessage={contactCodeErrMsg} onChangeText={this.onContactCodeChanged}/>
                    </View>
                    <Button title="Add friend" onPress={this.onAddContactPressed} />
                </View>
                <View flex={2}>
                    <Text style={styles.heading}>Give My Code to a Friend</Text>
                    <Button title="Get my contact code" onPress={this.onGetContactCodePressed}/>
                    <Input id="myContactCode" placeholder="My contact code" value={this.state.myContactCode} editable={false} leftIcon={<Icon name="key" />}/>
                    <Text>{this.state.myContactCodeExpiry}</Text>
                </View>
                <View flex={1} justifyContent="flex-end" alignContent="flex-end">
                    <Button title="Cancel" onPress={this.props.closeHandler} />
                </View>
            </Overlay>
        );
    }
}