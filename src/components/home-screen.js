import React from 'react';
import { View, Alert } from 'react-native';
import { SearchBar} from 'react-native-elements';
// import AddContact from './add-contact-overlay';
// import EditNickname from './edit-nickname-overlay';
import styles from '../styles/main';
// import {Database} from './../lib/realm-connection';
// import SocketClient from '../lib/socket-client';
// import AddContactButton from './add-contact-button';
// import ContactList from './contact-list';

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    // let allContacts = Database.GetAllContacts();
    // this.state = { addContactIsVisible: false, editNicknameIsVisible: false, contactCode: "", contacts:[], editableContact: null};
    // this.searchContacts = this.searchContacts.bind(this);
    // allContacts.addListener(()=>{
    //   let contacts = [];
    //   let contactIterator = allContacts.values();
    //   for(let contact of contactIterator){
    //     contacts.push(contact);
    //   }
    //   this.setState({contacts});
    // });
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    return{
      header: (
        <View style={styles.header}>
          <SearchBar placeholder="Search Contacts" onChangeText={(searchText) => params.onTextChange(searchText)}/>
        </View>
      )
    }
  };

  componentDidMount() {
    // this.props.navigation.setParams({ onTextChange: this.searchContacts, clearSearchText: this.clearSearchText, searchValue: "" });
  }

  searchContacts(searchText){
    // this.setState({contacts: Database.GetAllContacts(searchText)});
  }

  onAddContactPress() {
    this.setState({ addContactIsVisible: true });
  }

  onCloseAddContactPress() {
    this.setState({ addContactIsVisible: false });
  }

  onCloseEditNicknamePress(){
    this.setState({editNicknameIsVisible: false});
  }

  onAddNewContactPress(contact){
    // SocketClient.getIdentifier(contact.contactCode, (identifier)=>{
    //   try{
    //     // Database.SaveContact({nickname: contact.nickname, identifier});
    //     this.setState({ addContactIsVisible: false });
    //   }
    //   catch(err){
    //     Alert.alert(
    //       "Unable to save friend",
    //       err.message,
    //       [
    //         {text: "OK"}
    //       ]
    //     );
    //   }
    // });
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'flex-start' }}>
        {/* <AddContactButton onAddContactPress={() => this.onAddContactPress()}/>
        <ContactList
          contacts={this.state.contacts}
          onContactPress={(contact)=>{this.props.navigation.navigate('Contact', {
              contact
            });
          }}
          editContactHandler={(contact)=>{
            this.setState({editNicknameIsVisible: true, editableContact: contact});
          }}
          deleteContactHandler={(contact)=>{
              Alert.alert(
                'Delete Contact',
                'Are you sure?',
                [
                  {text: 'Cancel', style: 'cancel'},
                  {text: 'OK', onPress: () =>Database.RemoveContact(contact)},
                ],
                { cancelable: true }
              )
            }
          }
          clearMessagesHandler={(contact)=>Database.RemoveContactMessages(contact)}
        />
        <AddContact
          isVisible={this.state.addContactIsVisible}
          closeHandler={() => this.onCloseAddContactPress()}
          backHandler={() =>{
              if (this.state.addContactIsVisible) {
                this.setState({addContactIsVisible: false});
                return true;
              }
              return false;
          }}
          addContactHandler={(contact) =>this.onAddNewContactPress(contact)}
        />
        <EditNickname
          isVisible={this.state.editNicknameIsVisible}
          editContactHandler={(newNickname)=>{
              Database.UpdateContact(this.state.editableContact.identifier, {nickname: newNickname})
              this.onCloseEditNicknamePress();
            }
          }
          nickname={this.state.editableContact ? this.state.editableContact.nickname: null}
          closeHandler={() => this.onCloseEditNicknamePress()}
          backHandler={() =>{
              if (this.state.editNicknameIsVisible) {
                this.setState({editNicknameIsVisible: false});
                return true;
              }
              return false;
          }}
        /> */}
      </View>
    );
  }
}