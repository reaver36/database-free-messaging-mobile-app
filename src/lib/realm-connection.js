// import Realm from 'realm';

// const ContactSchema = {
//     name: 'contact',
//     primaryKey: 'identifier',
//     properties: {
//         nickname:  'string',
//         identifier: 'string',
//         messages: 'message[]'
//     }
// };

// const MessageSchema = {
//     name: 'message',
//     primaryKey: 'id',
//     properties: {
//         id: 'int',
//         text:  'string',
//         createdAt: 'date',
//         sentRecieve: 'string'
//     }
// };

// const KEY = "db-free-msging.realm-connection";

// if (!global[KEY]){
//   global[KEY] = new Realm({schema: [ContactSchema, MessageSchema]});
// }

// var singleton = {};

// Object.defineProperty(singleton, "instance", {
//   get: function(){
//     return global[KEY];
//   }
// });

// Object.freeze(singleton);

// const RealmConnection = singleton.instance;

// export const SENT_RECIEVE = {SENT:'sent', RECIEVE: 'recieve'};

// export class Database{
//     // Create a new contact or update if contact is a valid realm object
//     static SaveContact(contact){
//         if(RealmConnection.objectForPrimaryKey('contact', contact.identifier)){
//             throw new Error("Friend already added");
//         }
//         let contactObj;
//         RealmConnection.write(() => {
//             contactObj = RealmConnection.create('contact', contact);
//         });
//         return contactObj
//     }

//     static UpdateContact(identifier, newProps){
//         RealmConnection.write(() => {
//             RealmConnection.create('contact', {identifier, ...newProps}, true);
//         });
//     }
    
//     static GetAllContacts(searchTerm){
//         let allContacts = RealmConnection.objects('contact');
//         if(!searchTerm || !searchTerm.length){
//             return allContacts;
//         }
//         return allContacts.filtered('nickname CONTAINS[c] $0', searchTerm);
//     }

//     static GetContactByIdentifier(identifier){
//         return RealmConnection.objects('contact').filtered('identifier == $0', identifier);
//     }

//     static RemoveContact(contact){
//         this.RemoveContactMessages(contact);// Need to delete messages first to avoid this bug in navigation https://github.com/realm/realm-js/issues/1031
//         RealmConnection.write(() => {
//             RealmConnection.delete(contact);
//         });
//     }

//     static SaveMessage(contact, message){
//         RealmConnection.write(() => {
//             message.id = RealmConnection.objects('message').max("id");
//             if(!message.id){
//                 message.id = 0;
//             }
//             message.id++;
//             contact.messages.push(message);
//         });
//     }

//     static RemoveContactMessages(contact){
//         RealmConnection.write(() => {
//             RealmConnection.delete(contact.messages);
//         });
//     }
// }