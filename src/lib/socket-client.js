import { SERVER_URL, SERVER_PORT } from 'react-native-dotenv'
import io from 'socket.io-client';

// Create a singleton so only one connection ever exists in the app
const KEY = "db-free-msging.socket-client";

if (!global[KEY]){
  global[KEY] = io(SERVER_URL + ":" + SERVER_PORT);
}

var singleton = {};

Object.defineProperty(singleton, "instance", {
  get: function(){
    return global[KEY];
  }
});

Object.freeze(singleton);

let socketClient = singleton.instance;
export default class SocketClient {
    static connect(identifier, connectHandler){
        // Initial connection
        socketClient.emit("connectionComplete", identifier, function(response){
            connectHandler(response);//1 on success, 0 otherwise
        });
        // If conenction is lost send identifier again
        socketClient.on('reconnecting', function(){
            socketClient.emit("connectionComplete", identifier, function(response){
                connectHandler(response);//1 on success, 0 otherwise
            });
        });
    }

    static sendMessage(msg, to, messageHandler){
        socketClient.emit("sendMessage", {msg, to}, function(response){
            messageHandler(response);//1 on success, 0 otherwise
        });
    }

    static getContactCode(identifier, contactCodeHandler){
        socketClient.emit("getContactCode", identifier, function(response){
            contactCodeHandler(response);
        });
    }

    static getIdentifier(code, identifierHandler){
        socketClient.emit("getIdentifier", code, function(response){
            identifierHandler(response);
        });
    }

    static isOnline(identifier, onlineHandler){
        socketClient.emit("isOnline", identifier, function(response){
            onlineHandler(response);
        });
    }

    static onMessageHandler(handler){
        socketClient.on('message', handler);
    }

    static onErrorHandler(handler){
        socketClient.on('error', handler);
    }

    static onAppErrorHandler(handler){
        socketClient.on('appError', handler);
    }

    static onConnectErrorHandler(handler){
        socketClient.on('connect_error', handler);
    }
}