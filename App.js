import React from 'react';
import {Alert} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './src/components/home-screen';
// import ContactScreen from './src/components/contact-screen';
import DeviceInfo from 'react-native-device-info';
import SocketClient from './src/lib/socket-client';
// import {Database, SENT_RECIEVE} from './src/lib/realm-connection';

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    // Contact: {
    //   screen: ContactScreen,
    // },
  },
  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    console.log('starting app');
    SocketClient.connect(DeviceInfo.getUniqueID(), function (response) {
      if(!response){
        Alert.alert(
          "Cannot connect to server",
          "",
          [
            {text: "OK"}
          ]
        );
      }
    });

    SocketClient.onMessageHandler(function(message){
        // let contact = Database.GetContactByIdentifier(message.from)[0];
        // if(!contact){
        //   contact = Database.SaveContact({nickname: message.from, identifier: message.from});
        // }
        
        // Database.SaveMessage(contact, {text: message.msg, createdAt: new Date(), sentRecieve: SENT_RECIEVE.RECIEVE});
    });

    SocketClient.onConnectErrorHandler(function(){
      Alert.alert(
        "Cannot connect to server",
        "",
        [
          {text: "OK"}
        ]
      );
    });
    SocketClient.onAppErrorHandler(function(err){
      Alert.alert(
        err.message,
        "",
        [
          {text: "OK"}
        ]
      );
    });
  }

  render() {
    return <RootStack />;
  }
}