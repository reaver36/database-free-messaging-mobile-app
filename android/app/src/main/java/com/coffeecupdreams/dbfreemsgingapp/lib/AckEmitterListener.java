package com.coffeecupdreams.dbfreemsgingapp.lib;

import io.socket.client.Ack;
import io.socket.emitter.Emitter;

public interface AckEmitterListener extends Ack, Emitter.Listener {
    
}