package com.coffeecupdreams.dbfreemsgingapp.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Binder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.app.PendingIntent;
import android.content.Intent;
import android.app.Notification;
import android.app.NotificationManager;
import com.coffeecupdreams.dbfreemsgingapp.R;
import com.coffeecupdreams.dbfreemsgingapp.lib.SocketClient;
import com.coffeecupdreams.dbfreemsgingapp.lib.AckEmitterListener;
import com.coffeecupdreams.dbfreemsgingapp.db.RealmConnection;
import java.net.URISyntaxException;

public class MessageConnectionService extends Service {
  private static final int NOTIF_ID = 1;
  private SocketClient socket;
  private final IBinder mBinder = new LocalBinder();

  public MessageConnectionService(Context applicationContext) throws URISyntaxException{
    super();
    socket = SocketClient.getInstance();
  }

  public MessageConnectionService() throws URISyntaxException{
    socket = SocketClient.getInstance();
  }

  private Notification getMyActivityNotification(CharSequence title, CharSequence text){
    // The PendingIntent to launch our activity if the user selects
    // this notification
    PendingIntent contentIntent = PendingIntent.getActivity(this,
            0, new Intent(this, MessageConnectionService.class), 0);

    return new Notification.Builder(this)
            .setContentTitle(title)
            .setContentText(text)
            .setSmallIcon(R.drawable.node_modules_reactnavigation_src_views_assets_backicon)
            .setContentIntent(contentIntent).getNotification();     
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
      super.onStartCommand(intent, flags, startId);
      Log.d("service", "in connect to service");
        // Init realm database for entire app
        RealmConnection.initRealm(this);
        socket.connect("somekey", new AckEmitterListener(){
          @Override
          public void call(Object... args){
            // Successfully connected
            if(args[0] instanceof Integer && (Integer)args[0] == 1){
                return;
            }
            Log.d("service", "in connect to service response: " + args[0].toString());
            //Failed to connect
            Notification notification = getMyActivityNotification(getText(R.string.background_connection_service), getText(R.string.service_no_connection));

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(NOTIF_ID, notification);
          }
      });
      startForeground(NOTIF_ID, getMyActivityNotification(getText(R.string.background_connection_service), getText(R.string.service_connected)));
      return START_STICKY;
  }

  @Override
  public void onDestroy() {
      super.onDestroy();
      Log.i("EXIT", "ondestroy!");
        Intent broadcastIntent = new Intent(this, MessageConnectionBroadcastReceiver.class);
      
      sendBroadcast(broadcastIntent);
      RealmConnection.closeRealm();
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
      return mBinder;
  }

  public String getWord(){
    return "test";
  }

  public class LocalBinder extends Binder {
    public MessageConnectionService getService() {
        return MessageConnectionService.this;
    }
}
}