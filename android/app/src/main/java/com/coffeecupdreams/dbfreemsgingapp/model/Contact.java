package com.coffeecupdreams.dbfreemsgingapp.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.RealmList;

public class Contact extends RealmObject{

    public String nickname;
    @PrimaryKey
    public String identifier;
    public RealmList<Message> messages;
}