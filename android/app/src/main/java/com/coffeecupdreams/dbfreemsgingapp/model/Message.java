package com.coffeecupdreams.dbfreemsgingapp.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import java.util.Date;

public class Message extends RealmObject{

    @PrimaryKey
    public long id;
    
    public String text;
    public Date createdAt;
    public String sentRecieve;
}