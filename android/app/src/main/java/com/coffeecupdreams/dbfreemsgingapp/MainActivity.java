package com.coffeecupdreams.dbfreemsgingapp;

import com.facebook.react.ReactActivity;
import java.net.URISyntaxException;
import com.coffeecupdreams.dbfreemsgingapp.service.MessageConnectionService;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.content.Context;
import android.widget.Toast;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.os.IBinder;
import android.os.Binder;
import android.util.Log;

public class MainActivity extends ReactActivity {
  Intent mServiceIntent;
  private MessageConnectionService mService;
  boolean mBound = false;
  private ServiceConnection mConnection = new ServiceConnection() {

    @Override
    public void onServiceConnected(ComponentName className,
            IBinder service) {
        Log.d("service", "service connected");
        // We've bound to LocalService, cast the IBinder and get LocalService instance
        MessageConnectionService.LocalBinder binder = (MessageConnectionService.LocalBinder) service;
        mService = binder.getService();
        mBound = true;
        Log.d("service", mService.getWord());
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
        mBound = false;
    }
};
  /**
   * Returns the name of the main component registered from JavaScript.
   * This is used to schedule rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "dbfreemsgingapp";
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.d("service", "onCreate");
    // try{
      // mService = new MessageConnectionService(this);
      mServiceIntent = new Intent(this, MessageConnectionService.class);
      if (!isMyServiceRunning(MessageConnectionService.class)) {
        startService(mServiceIntent);
      }
    // }
    // catch(URISyntaxException exception){
    //   Context context = getApplicationContext();

    //   Toast toast = Toast.makeText(context, R.string.unexpected_server_connection_error, Toast.LENGTH_SHORT);
    //   toast.show();
    // }
    Log.d("service", "onCreate end");
  }

  @Override
  protected void onStart(){
    super.onStart();
    Log.d("service", "onStart");
    bindService(mServiceIntent, mConnection, Context.BIND_AUTO_CREATE);
  }

  @Override
  protected void onStop() {
      super.onStop();
      unbindService(mConnection);
      mBound = false;
  }

  private boolean isMyServiceRunning(Class<?> serviceClass) {
    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
    for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
      if (serviceClass.getName().equals(service.service.getClassName())) {
        return true;
      }
    }
    return false;
  }


  @Override
  protected void onDestroy() {
    stopService(mServiceIntent);
    super.onDestroy();
  }
}
