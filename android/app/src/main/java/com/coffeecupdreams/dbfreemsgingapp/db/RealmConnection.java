package com.coffeecupdreams.dbfreemsgingapp.db;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Case;
import android.content.Context;
import com.coffeecupdreams.dbfreemsgingapp.model.*;
import com.coffeecupdreams.dbfreemsgingapp.db.exception.*;

public class RealmConnection{
    private static Realm realm;

    public static void initRealm(Context context){
        Realm.init(context);
        realm = Realm.getDefaultInstance();
    }

    public static void closeRealm(){
        realm.close();
    }

    public static void AddContact(Contact contact) throws DuplicateKeyException{
        
        if(realm.where(Contact.class).equalTo("identifier", contact.identifier).findFirst() != null){
            throw new DuplicateKeyException("Contact already added");
        }
        realm.insert(contact);
    }

    public static void UpdateContact(Contact contact){
        realm.insertOrUpdate(contact);
    }
    
    public static RealmResults<Contact> GetAllContacts(){
        return realm.where(Contact.class).findAll();
    }

    public static RealmResults<Contact> GetAllContacts(String searchTerm){
        return realm.where(Contact.class).contains("nickname", searchTerm, Case.INSENSITIVE).findAll().sort("nickname");
    }

    public static Contact GetContactByIdentifier(String identifier){
        return realm.where(Contact.class).equalTo("identifier", identifier).findFirst();
    }

    public static void RemoveContact(String identifier){
        realm.beginTransaction();
        try{
            Contact contact = GetContactByIdentifier(identifier);
            RemoveContactMessages(contact);
            contact.deleteFromRealm();
            realm.commitTransaction();
        }
        catch(Exception ex){
            realm.cancelTransaction();
            throw ex;
        }
    }

    public static void SaveMessage(String identifier, Message message){
        Contact contact = realm.where(Contact.class).equalTo("identifier", identifier).findFirst();
        Number maxId = realm.where(Message.class).findAll().max("id");
        message.id = 0;
        if(maxId != null){
            message.id = maxId.longValue() + 1;
        }
        realm.beginTransaction();
        try{
            realm.insert(message);
            contact.messages.add(message);
            realm.commitTransaction();
        }
        catch(Exception ex){
            realm.cancelTransaction();
            throw ex;
        }
    }

    public static void RemoveContactMessages(Contact contact){
        contact.messages.deleteAllFromRealm();
    }
}