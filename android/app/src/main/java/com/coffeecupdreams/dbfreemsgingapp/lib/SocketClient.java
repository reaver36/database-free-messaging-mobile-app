package com.coffeecupdreams.dbfreemsgingapp.lib;

import io.socket.client.IO;
import io.socket.client.Socket;
import java.net.URISyntaxException;
import com.google.gson.Gson;
import android.util.Log;

public class SocketClient{
  private static SocketClient instance;
  private static String server = "http://10.0.2.2:3000";//TODO: Update to use config optinos
  private static final String CONNECTION_COMPLETE = "connectionComplete";
  private static final String SEND_MESSAGE = "sendMessage";
  private static final String GET_CONTACT_CODE = "getContactCode";
  private static final String GET_IDENTIFIER = "getIdentifier";
  private static final String IS_ONLINE = "isOnline";
  private static final String MESSAGE = "message";
  private static final String APP_ERROR = "appError";
  private Socket socketClient;

  private SocketClient() throws URISyntaxException{
    socketClient = IO.socket(server);
  }

  public static SocketClient getInstance() throws URISyntaxException{
    if(instance == null){
      instance = new SocketClient();
    }
    return instance;
  }

  public void connect(String identifier, AckEmitterListener connectHandler){
    // If conenction is lost send identifier again
    AckEmitterListener connectListener = new ConnectionListener(identifier, connectHandler);
    Log.d("service", "in connect to socket");
    socketClient.on(Socket.EVENT_CONNECT_ERROR, connectHandler
    ).on(Socket.EVENT_RECONNECT_ERROR, connectHandler
    ).on(Socket.EVENT_CONNECT, connectListener
    ).on(Socket.EVENT_RECONNECT, connectListener);
    socketClient.connect();
  }

  public void sendMessage(String msg, String to, AckEmitterListener messageHandler){
    Gson gson = new Gson();
    socketClient.emit(SEND_MESSAGE, gson.toJson(new Message(msg, to)), messageHandler);
  }

  public void getContactCode(String identifier, AckEmitterListener contactCodeHandler){
    socketClient.emit(GET_CONTACT_CODE, identifier, contactCodeHandler);
  }

  public void getIdentifier(String code, AckEmitterListener identifierHandler){
    socketClient.emit(GET_IDENTIFIER, code, identifierHandler);
  }

  public void isOnline(String identifier, AckEmitterListener onlineHandler){
    socketClient.emit(IS_ONLINE, identifier, onlineHandler);
  }

  public void onMessageHandler(AckEmitterListener handler){
    socketClient.on(MESSAGE, handler);
  }

  public void onErrorHandler(AckEmitterListener handler){
    socketClient.on(Socket.EVENT_ERROR, handler);
  }

  public void onAppErrorHandler(AckEmitterListener handler){
    socketClient.on(APP_ERROR, handler);
  }

  public void onConnectErrorHandler(AckEmitterListener handler){
    socketClient.on(Socket.EVENT_CONNECT_ERROR, handler);
  }
  
  class Message{
    public String msg;
    public String identifier;
    
    public Message(String msg, String identifier){
      this.msg = msg;
      this.identifier = identifier;
    }
  }

  private class ConnectionListener implements AckEmitterListener {
    private String identifier;
    private AckEmitterListener connectHandler;

    public ConnectionListener(String identifier, AckEmitterListener connectHandler){
      this.identifier = identifier;
      this.connectHandler = connectHandler;
    }

    @Override
    public void call(Object... args) {
      // Initial connection
      socketClient.emit(CONNECTION_COMPLETE, identifier, connectHandler);
    }
  }
}